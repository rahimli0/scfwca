<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Announce */

$this->title = Yii::t('backend', 'Create Announce');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Announces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="announce-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
