<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #user-password {
        float: left;
        width: 80% !important;
    }
</style>
<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>
    <label>password </label>
    <?= $form->field($model, 'password')->textInput()->label(false) ?>

    <?= $form->field($model, 'status')->dropDownList(['' => 'Status', 0 => 'No Active', 10 => 'Active']); ?>

    <?= $form->field($model, 'is_staff')->checkbox() ?>

    <?= $form->field($model, 'is_superuser')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $( '<a class="btn btn-danger" onclick="passwordGenerator()">password_generator</a>' ).insertAfter( "#user-password" );
    function generatePassword() {
        var length = 16,
            charset = "!@#$%^&*abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
    function passwordGenerator() {
        $('#user-password').val(generatePassword());
    }
</script>