<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'SCFWCA';
?>
<!-- Content Wrapper. Contains page content -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
            ['Dates', 'Last 24 hours', 'Last 7 days', 'Last 1 month', 'Last 1 Years'],
            ['Dates',  <?= $data_post['last_24'];?>,<?= $data_post['last_7'];?>,<?= $data_post['last_30'];?>,<?= $data_post['last_365'];?>]
        ]);

        var options = {
            title : 'Read Post daily ,weekly , monthly, yearly',
            vAxis: {title: 'Count'},
            hAxis: {title: ''},
            seriesType: 'bars',
            series: {5: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'Sales'],
            <?php foreach ($data_years as $key => $data_year){?>
            [<?= $data_year['year']?>,  <?= $data_year['count']?>],
            <?php }?>
        ]);

        var options = {
            title: '<?= Yii::t('backend', 'Appeals'); ?>',
            curveType: 'function',
            legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
    }
</script>
<div class="content-wrapper" style="margin: 0;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= Yii::t('backend', 'Dashboard'); ?>
            <small><?= Yii::t('backend', 'Control panel'); ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> <?= Yii::t('backend', 'Home'); ?></a></li>
            <li class="active"><?= Yii::t('backend', 'Dashboard'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?= $questions_count;?></h3>

                        <p><?= Yii::t('backend', 'Questions'); ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="<?php echo Url::toRoute('question/index') ?>" class="small-box-footer"><?= Yii::t('backend', 'Show More'); ?><i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?= $menus_count;?></h3>

                        <p><?= Yii::t('backend', 'Menus'); ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="<?php echo Url::toRoute('menu/index') ?>" class="small-box-footer">
                        <?= Yii::t('backend', 'Show More'); ?> <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $posts_count;?></h3>

                        <p><?= Yii::t('backend', 'Posts'); ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo Url::toRoute('post/index') ?>" class="small-box-footer">
                        <?= Yii::t('backend', 'Show More'); ?> <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?= $appeals_count ;?></h3>

                        <p><?= Yii::t('backend', 'Appeals'); ?></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="<?php echo Url::toRoute('appeal/index') ?>" class="small-box-footer">
                        <?= Yii::t('backend', 'Show More'); ?>  <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div id="chart_div" style="width: 100%; height: 500px;"></div>
<hr>

                <div id="curve_chart" style="width: 100%; height: 500px"></div>
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--<script src="--><?//= Yii::getAlias('@backend_web') ?><!--/custom_base/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<!--<script src="--><?//= Yii::getAlias('@backend_web') ?><!--/custom_base/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->