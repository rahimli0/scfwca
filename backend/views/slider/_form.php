<?php

use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<link rel="stylesheet" href="/addmin_static/croper/cropper.css"/>
<link rel="stylesheet" href="/addmin_static/croper/cropper-admin-main.css"/>
<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'title_az')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <div class="portlet-body">
        <!-- BEGIN FORM-->
        <div class="form-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="img-container">
                        <img id="main-image" src="">
                    </div>
                </div>
                <div class="col-md-6">
                    <img id="cropped-main-image" src="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <img id="result-cropped-main-image" src="<?php if (!$model->isNewRecord) {
                        echo $model->image;
                    } ?>" class="preview-sm">
                    <input type="hidden" id="hidden-result-cropped-main-image" name="result_cropped_main_image"
                           value=""/>
                    <input type="hidden" id="hidden-original-main-image" value=""/>
                </div>
            </div>
            <div class="row" id="actions">
                <div class="col-md-9 docs-buttons">
                    <!-- <h3>Toolbar:</h3> -->
                    <div class="btn-group">
                        <a href="javascript:void(0)" class="btn btn-info" data-method="zoom" data-option="0.1"
                           title="Zoom In">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                                            <span class="fa fa-search-plus"></span>
                                        </span>
                        </a>
                        <a href="javascript:void(0)" class="btn btn-info" data-method="zoom" data-option="-0.1"
                           title="Zoom Out">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                                            <span class="fa fa-search-minus"></span>
                                        </span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="-10"
                           data-second-option="0"
                           title="Move Left">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
                                            <span class="fa fa-arrow-left"></span>
                                        </span>
                        </a>
                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="10"
                           data-second-option="0"
                           title="Move Right">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
                                            <span class="fa fa-arrow-right"></span>
                                        </span>
                        </a>
                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="0"
                           data-second-option="-10"
                           title="Move Up">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
                                            <span class="fa fa-arrow-up"></span>
                                        </span>
                        </a>
                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="0"
                           data-second-option="10"
                           title="Move Down">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
                                            <span class="fa fa-arrow-down"></span>
                                        </span>
                        </a>
                    </div>

                    <div class="btn-group">
                        <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                            <input type="file" class="sr-only" id="inputImage" name="file"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">

                            <span class="docs-tooltip" data-toggle="tooltip" title="Şəkil Seç">
                                            <span class="fa fa-upload"></span>
                                        </span>
                        </label>
                    </div>
                    <div class="btn-group btn-group-crop">
                        <a href="javascript:void(0)" class="btn btn-primary" data-method="getCroppedCanvas"
                           data-option="{ &quot;width&quot;: 757, &quot;height&quot;: 400 }">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="Kəs və göstər">
                                            Kəs və göstər
                                        </span>
                        </a>
                    </div>
                    <!-- Show the cropped image in modal -->
                    <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true"
                         aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a href="javascript:void(0)" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                                    <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <a href="javascript:void(0)" class="btn btn-default" data-dismiss="modal">Close</a>
                                    <a href="javascript:void(0)" class="btn btn-primary" id="download"
                                       href="javascript:void(0);"
                                       download="cropped.jpg">Download</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal -->
                </div><!-- /.docs-buttons -->
            </div>
        </div>
    </div>


    <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_index')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(['' => 'Status', 'big' => 'Big Slider', 'small' => 'Small Slider']); ?>

    <?= $form->field($model, 'is_active')->textInput()->checkBox(['checked' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    document.getElementById('slider-image').setAttribute("readonly", true);
</script>

<script src="/addmin_static/js/jquery.min.js"></script>
<script src="/addmin_static/croper/cropper-custom-main.js"></script>
<script src="/addmin_static/croper/cropper.js"></script>