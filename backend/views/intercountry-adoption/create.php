<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\IntercountryAdoption */

$this->title = Yii::t('backend', 'Create Intercountry Adoption');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Intercountry Adoptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intercountry-adoption-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
