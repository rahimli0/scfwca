<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            ['attribute' => 'menu',
//                'value' => 'menu0.name_az',
//            ],
            ['attribute' => 'name_az',
                'value' => 'name_az',
            ],
//            'menu',
//            'name',
            'name_en',
             'name_ru',
            // 'show_homepage',
            // 'image',
            // 'short_text_az',
            // 'short_text_en',
            // 'short_text_ru',
            // 'context_az:ntext',
            // 'context_en:ntext',
            // 'context_ru:ntext',
            // 'active',
            // 'slug',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script>
    $("tbody tr td").each(function() {
        var htmlRegex = new RegExp("<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>");
        if($(this).html().indexOf("<a") < 0){
        // Within tr we find the last td child element and get content
        $(this).text($(this).text().slice(0, 25));
        }
    });
</script>