<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PostMenu */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Post Menu',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Post Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="post-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
