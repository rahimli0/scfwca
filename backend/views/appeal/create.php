<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Appeal */

$this->title = Yii::t('backend', 'Create Appeal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Appeals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appeal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
