<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Social */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->dropDownList(['' => 'Social', 'facebook' => 'Facebook', 'twitter' => 'Twitter', 'youtube' => 'Youtube', 'instagram' => 'Instagram', 'gplus' => 'Google Plus', 'dribbble' => 'Dribbble', 'linkedin' => 'Linkedin', 'dropbox' => 'Dropbox', 'wordpress' => 'Wordpress', 'pinterest' => 'Pinterest', 'blogger' => 'Blogger', 'evernote' => 'Evernote', 'vimeo' => 'Vimeo']); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_index')->textInput() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
