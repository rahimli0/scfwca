<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PostView */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-view-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>
    <?php $postArray = ArrayHelper::map(\backend\models\Post::find()->orderBy('name_az')->all(), 'id', 'name_az') ?>


    <?php echo $form->field($model, 'post_id')->widget(Select2::classname(), [
        'data' => $postArray,
        'options' => ['placeholder' => '-- Choose Post --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Post');
    ?>

    <?php $menuArray = ArrayHelper::map(\backend\models\Menu::find()->orderBy('name_az')->all(), 'id', 'name_az') ?>

    <?php echo $form->field($model, 'menu_id')->widget(Select2::classname(), [
        'data' => $menuArray,
        'options' => ['placeholder' => '-- Choose Menu --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Menu');
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
