<?php

namespace backend\models;

use Yii;
use yii\helpers\Url;
use cornernote\linkall\LinkAllBehavior;
/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_az
 * @property string $name_en
 * @property string $name_ru
 * @property string $image
 * @property integer $show_homepage
 * @property integer $show_comment
 * @property integer $active
 * @property string $short_text_az
 * @property string $short_text_en
 * @property string $short_text_ru
 * @property string $context_az
 * @property string $context_en
 * @property string $context_ru
 * @property integer $read_count
 * @property string $slug
 * @property string $date
 * @property string $end_date
 *
 * @property Menu $menu0
 * @property PostImage[] $postImages
 * @property PostView[] $postViews
 */
class Post extends \yii\db\ActiveRecord
{
//    public $imageFiles;
    /**
     * @inheritdoc
     */
    public $imageFiles;
    public $menu_ids;
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_ids', 'name', 'slug', 'date'], 'required'],
            [[ 'show_homepage','show_comment', 'active','read_count'], 'integer'],
            [['context_az', 'context_en', 'context_ru'], 'string'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, bmp ', 'maxFiles' => 100],
            [['date','end_date',], 'safe'],
            [['name', 'name_az', 'name_en', 'name_ru', 'slug'], 'string', 'max' => 255],
            [['image', 'short_text_az', 'short_text_en', 'short_text_ru'], 'string', 'max' => 500],
//            [['menu'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
//            'menu' => Yii::t('backend', 'Menu'),
            'name' => Yii::t('backend', 'Name'),
            'name_az' => Yii::t('backend', 'Name Az'),
            'name_en' => Yii::t('backend', 'Name En'),
            'name_ru' => Yii::t('backend', 'Name Ru'),
            'image' => Yii::t('backend', 'Image'),
            'show_homepage' => Yii::t('backend', 'Show Homepage'),
            'show_comment' => Yii::t('backend', 'Forum'),
            'active' => Yii::t('backend', 'Active'),
            'short_text_az' => Yii::t('backend', 'Short Text Az'),
            'short_text_en' => Yii::t('backend', 'Short Text En'),
            'short_text_ru' => Yii::t('backend', 'Short Text Ru'),
            'context_az' => Yii::t('backend', 'Context Az'),
            'context_en' => Yii::t('backend', 'Context En'),
            'context_ru' => Yii::t('backend', 'Context Ru'),
            'read_count' => Yii::t('backend', 'Read Count'),
            'slug' => Yii::t('backend', 'Slug'),
            'end_date' => Yii::t('backend', 'End date'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }



    public function behaviors()
    {
        return [
            LinkAllBehavior::className(),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $menus = [];
        foreach ($this->menu_ids as $menu_id) {
            $menu = Menu::getMenuByID($menu_id);
            if ($menu) {
                $menus[] = $menu;
            }
        }
        $this->linkAll('menus', $menus);
        parent::afterSave($insert, $changedAttributes);
    }

    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['id' => 'menu_id'])
            //->via('postToTag');
            ->viaTable('post_menu', ['post_id' => 'id']);
    }
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getMenu0()
//    {
//        return $this->hasOne(Menu::className(), ['id' => 'menu']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPimages()
    {
        return $this->hasMany(PostImage::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostViews()
    {
        return $this->hasMany(PostView::className(), ['post_id' => 'id']);
    }

    public function upload()
    {
        if ($this->validate()) {
//            foreach ($this->imageFiles as $file) {
//                $file->saveAs(Url::to('@frontend/web/post-image/') . $file->baseName . '.' . $file->extension);
//            }
            return true;
        } else {
            return false;
        }
    }

}
