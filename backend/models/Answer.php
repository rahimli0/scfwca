<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%answer}}".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $title_az
 * @property string $title_en
 * @property string $title_ru
 * @property integer $active
 * @property string $date
 *
 * @property Question $question
 * @property QuestionResult[] $questionResults
 */
class Answer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'title_az', 'title_en', 'title_ru'], 'required'],
            [['question_id', 'active'], 'integer'],
            [['date'], 'safe'],
            [['title_az', 'title_en', 'title_ru'], 'string', 'max' => 500],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'question_id' => Yii::t('backend', 'Question ID'),
            'title_az' => Yii::t('backend', 'Title Az'),
            'title_en' => Yii::t('backend', 'Title En'),
            'title_ru' => Yii::t('backend', 'Title Ru'),
            'active' => Yii::t('backend', 'Active'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionResults()
    {
        return $this->hasMany(QuestionResult::className(), ['answer_id' => 'id']);
    }
}
