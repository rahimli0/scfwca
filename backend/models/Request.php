<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $question
 * @property string $date
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'email', 'question'], 'required'],
            [['question'], 'string'],
            [['date'], 'safe'],
            ['email', 'email'],
            [['first_name', 'last_name', 'phone', 'email'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'string', 'min' => 3],
            [['phone'], 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'phone' => Yii::t('backend', 'Phone'),
            'email' => Yii::t('backend', 'Email'),
            'question' => Yii::t('backend', 'Question'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
