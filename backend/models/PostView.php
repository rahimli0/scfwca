<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "post_view".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $post_id
 * @property integer $menu_id
 * @property string $date
 */
class PostView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'post_id'], 'required'],
            [['post_id', 'menu_id'], 'integer'],
            [['date'], 'safe'],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'ip' => Yii::t('backend', 'Ip'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'menu_id' => Yii::t('backend', 'Menu ID'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
