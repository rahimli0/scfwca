<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%video}}".
 *
 * @property integer $id
 * @property string $title_az
 * @property string $title_en
 * @property string $title_ru
 * @property string $video_url
 * @property integer $active
 * @property string $date
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_az', 'title_en', 'title_ru', 'video_url'], 'required'],
            [['active'], 'integer'],
            [['date'], 'safe'],
            [['title_az', 'title_en', 'title_ru'], 'string', 'max' => 255],
            [['video_url'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title_az' => Yii::t('backend', 'Title Az'),
            'title_en' => Yii::t('backend', 'Title En'),
            'title_ru' => Yii::t('backend', 'Title Ru'),
            'video_url' => Yii::t('backend', 'Video Url'),
            'active' => Yii::t('backend', 'Active'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
