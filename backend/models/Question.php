<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%question}}".
 *
 * @property integer $id
 * @property string $title_az
 * @property string $title_en
 * @property string $title_ru
 * @property integer $active
 * @property string $date
 *
 * @property Answer[] $answers
 * @property QuestionResult[] $questionResults
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%question}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_az', 'title_en', 'title_ru'], 'required'],
            [['active'], 'integer'],
            [['date'], 'safe'],
            [['title_az', 'title_en', 'title_ru'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title_az' => Yii::t('backend', 'Title Az'),
            'title_en' => Yii::t('backend', 'Title En'),
            'title_ru' => Yii::t('backend', 'Title Ru'),
            'active' => Yii::t('backend', 'Active'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionResults()
    {
        return $this->hasMany(QuestionResult::className(), ['question_id' => 'id']);
    }
}
