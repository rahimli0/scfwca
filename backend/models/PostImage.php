<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%post_image}}".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $image_url
 * @property string $date
 *
 * @property Post $post
 */
class PostImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'image_url'], 'required'],
            [['post_id'], 'integer'],
            [['date'], 'safe'],
            [['image_url'], 'string', 'max' => 500],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'image_url' => Yii::t('backend', 'Image Url'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
