<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $title_az
 * @property string $title_en
 * @property string $title_ru
 * @property string $image
 * @property string $url
 * @property integer $order_index
 * @property integer $is_active
 * @property string $type
 * @property string $date
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'url', 'order_index', 'is_active', 'type'], 'required'],
            [['order_index', 'is_active'], 'integer'],
            [['date'], 'safe'],
            [['title_az', 'title_en', 'title_ru'], 'string', 'max' => 255],
            [['image', 'url'], 'string', 'max' => 500],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title_az' => Yii::t('backend', 'Title Az'),
            'title_en' => Yii::t('backend', 'Title En'),
            'title_ru' => Yii::t('backend', 'Title Ru'),
            'image' => Yii::t('backend', 'Image'),
            'url' => Yii::t('backend', 'Url'),
            'order_index' => Yii::t('backend', 'Order Index'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'type' => Yii::t('backend', 'Type'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
