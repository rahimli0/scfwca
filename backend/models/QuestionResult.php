<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%question_result}}".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $answer_id
 * @property string $user_ip
 * @property string $date
 *
 * @property Answer $answer
 * @property Question $question
 */
class QuestionResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%question_result}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'answer_id', 'user_ip'], 'required'],
            [['question_id', 'answer_id'], 'integer'],
            [['date'], 'safe'],
            [['user_ip'], 'string', 'max' => 100],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'question_id' => Yii::t('backend', 'Question ID'),
            'answer_id' => Yii::t('backend', 'Answer ID'),
            'user_ip' => Yii::t('backend', 'User Ip'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }
}
