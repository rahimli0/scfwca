<?php

namespace backend\controllers;

use Yii;
use backend\models\IntercountryAdoption;
use backend\models\IntercountryAdoptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IntercountryAdoptionController implements the CRUD actions for IntercountryAdoption model.
 */
class IntercountryAdoptionController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IntercountryAdoption models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new IntercountryAdoptionSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Displays a single IntercountryAdoption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Creates a new IntercountryAdoption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = new IntercountryAdoption();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Updates an existing IntercountryAdoption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Deletes an existing IntercountryAdoption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Finds the IntercountryAdoption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IntercountryAdoption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntercountryAdoption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
