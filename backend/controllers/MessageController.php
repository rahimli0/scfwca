<?php

namespace backend\controllers;

use Yii;
use backend\models\Message;
use backend\models\MessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MessageController implements the CRUD actions for Message model.
 */
    class MessageController extends Controller
    {
        /**
         * @inheritdoc
         */
        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }

        /**
         * Lists all Message models.
         * @return mixed
         */
        public function actionIndex()
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new MessageSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Displays a single Message model.
         * @param integer $id
         * @param string $language
         * @return mixed
         */
        public function actionView($id, $language)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id, $language),
            ]);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Creates a new Message model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = new Message();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Updates an existing Message model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @param string $language
         * @return mixed
         */
        public function actionUpdate($id, $language)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = $this->findModel($id, $language);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Deletes an existing Message model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @param string $language
         * @return mixed
         */
        public function actionDelete($id, $language)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id, $language)->delete();

            return $this->redirect(['index']);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Finds the Message model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @param string $language
         * @return Message the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id, $language)
        {
            if (($model = Message::findOne(['id' => $id, 'language' => $language])) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
